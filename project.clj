(defproject hello "0.1.0-SNAPSHOT"
  :description "Stupidly simple HTTP server"
  :url "https://github.com/topikettunen"
  :license {:name "EPL-2.0"
            :url "https://www.eclipse.org/legal/epl-2.0/"}
  :dependencies [[org.clojure/clojure "1.9.0"]
                 [org.clojure/data.json "0.2.6"]
                 [org.clojure/tools.cli "0.4.1"]
                 [org.clojure/tools.logging "0.4.1"]
                 [http-kit "2.3.0"]
                 [compojure "1.6.1"]
                 [ring-cors "0.1.13"]]
  :main ^:skip-aot hello.core
  :target-path "target/%s"
  :profiles {:uberjar {:aot :all}})
