(ns hello.core
  (:require [clojure.string :as string]
            [clojure.data.json :as json]
            [clojure.tools.cli :refer [parse-opts]]
            [clojure.tools.logging :as log]
            [org.httpkit.server :refer :all]
            [compojure.core :refer :all]
            [compojure.route :as route]
            [ring.middleware.cors :refer [wrap-cors])
  (:gen-class))

(def cli-options
  [["-H" "--hostname HOST"
    :default "0.0.0.0"]
   ["-p" "--port PORT" "Port number"
    :default 5000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   ["-h" "--help"]])

(defn usage [options-summary]
  (->> ["Hello-HTTP - Stupidly simple HTTP server"
        ""
        "Usage: hello-http [options]"
        ""
        options-summary]
       (string/join \newline)))

(defn hello-handler [req]
  {:status 200
   :headers {"Content-Type" "application/json"}
   :body (json/write-str {:message "Hello, world!"})})

(defn async-handler [ring-request]
  (with-channel ring-request channel
    (if (websocket? channel)
      (on-receive channel (fn [data]
                            (send! channel data)))
      (send! channel {:status 200
                      :headers {"Content-Type" "application/json"}
                      :body (json/write-str {:message "Hello, async!"})}))))

(defn fault-handler [req]
  {:status 400
   :headers {"Content-Type" "application/json"}
   :body (json/write-str {:error "not found"})})

(defroutes all-routes
  (GET "/" [] hello-handler)
  (GET "/async" [] async-handler)
  (route/not-found fault-handler))

(defn exit [status msg]
  (println msg)
  (System/exit status))

(def app
  (-> all-routes
    (wrap-cors
     :access-control-allow-origin [#".*"]
     :access-control-allow-headers ["Content-Type"]
     :access-control-allow-methods [:get :put :post :delete :options])))

(defn -main [& args]
  (let [{:keys [options arguments summary errors]} (parse-opts args cli-options)]
    (cond
      (:help options) (exit 0 (usage summary)))
    (log/info (format "Starting stupidly simple HTTP server to http://%s:%s"
                      (:hostname options) (:port options)))
    (run-server #'app {:ip (:hostname options)
                            :port (:port options)})))
