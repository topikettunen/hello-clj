FROM clojure

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY project.clj /usr/src/app/

COPY src /usr/src/app/src

RUN lein deps

RUN mv "$(lein uberjar | sed -n 's/^Created \(.*standalone\.jar\)/\1/p')" hello-http.jar

EXPOSE 5000

ENTRYPOINT [ "java", "-jar", "hello-http.jar" ]
